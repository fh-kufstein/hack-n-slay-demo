using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New EnemyData", menuName = "Enemy Data", order = 51)]
public class EnemyScriptableObject : EnemyEntity
{    
    [SerializeField]
    private float pickupChance;
    [SerializeField]
    private GameObject[] pickups;

    public EnemyScriptableObject(float lifePoints, float armor, int attackSpeed, int damage, float moveSpeed) : base(lifePoints, armor, attackSpeed, damage, moveSpeed)
    {
    }

    public GameObject GetPickup()
    {
        if (Random.value * pickupChance > 0.5)
        {
            var pickup = pickups.GetValue(Random.Range(0, pickups.Length)) as GameObject;
            return pickup;
        }
        return null;
    }
}