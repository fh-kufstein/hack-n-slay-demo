using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField]
    private EnemyScriptableObject enemyData;
    [SerializeField]
    private GameObject coin;

    private PerishableEntityStats stats;

    private bool IsTouching = false;

    void Start()
    {
        stats = new PerishableEntityStats(enemyData);
        GetComponent<SpriteRenderer>().sprite = enemyData.Icon;
        GetComponent<Animator>().runtimeAnimatorController = enemyData.AnimationController;
        StartCoroutine(Attack());
    }

    void Update()
    {
        var before = transform.position.x;
        transform.position = Vector2.MoveTowards(transform.position, Camera.main.transform.position, enemyData.MoveSpeed * Time.deltaTime);
        
        GetComponent<SpriteRenderer>().flipX = before > transform.position.x;
    }

    public void OnDamage()
    {
        stats.TakeDamage(Game.Instance.Player.AttackDamage);

        if (stats.IsDead())
        {
            Destroy(gameObject);
            var pickup = enemyData.GetPickup();
            if (pickup != null)
            {
                Instantiate(pickup, gameObject.transform.position, Quaternion.identity);
            }


            Instantiate(coin, gameObject.transform.position, Quaternion.identity);
            Game.Instance.RemoveEnemy();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Player.ToString()))
        {
            IsTouching = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Player.ToString()))
        {
            IsTouching = false;
        }
    }

    private IEnumerator Attack()
    {
        while (true)
        {
            yield return new WaitForSeconds(.5f / enemyData.AttackSpeed);
            if (!IsTouching) continue;

            Game.Instance.Player.TakeDamage(enemyData.AttackDamage);

            if (Game.Instance.Player.IsDead())
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
