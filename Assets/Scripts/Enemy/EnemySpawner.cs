using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemyPrefab;


    void Start()
    {
        StartCoroutine(Spawn());
    }

    void Update()
    {

    }
    IEnumerator Spawn()
    {
        while (true)
        {
            var spawnFactor = 1 / Game.Instance.SpawnSpeed / (1 + Game.Instance.Score);
            yield return new WaitForSeconds(1f * spawnFactor);
            if(Time.timeScale == 0) //When Paused don't spawn 
            {
                continue;
            }

            if (Game.Instance.CurrentEnemyAmount > Game.Instance.MaxEnemyAmount) continue;
;
            var y = 0f;
            var x = 0f;

            switch(Random.Range(0,4))
            {
                case 0: //Spawn Top
                    y = Game.Instance.SpawnDistance;
                    x = Game.Instance.SpawnDistance * Random.Range(-1f, 1f);
                    break;
                case 1: //Spawn Bottom
                    y = -Game.Instance.SpawnDistance;
                    x = Game.Instance.SpawnDistance * Random.Range(-1f, 1f);
                    break;
                case 2: //Spawn Left
                    x = Game.Instance.SpawnDistance;
                    y = Game.Instance.SpawnDistance * Random.Range(-1f, 1f);
                    break;
                case 3: //Spawn Right
                    x = -Game.Instance.SpawnDistance;
                    y = Game.Instance.SpawnDistance * Random.Range(-1f, 1f);
                    break;

            }

            var enemy = Instantiate(enemyPrefab[Random.Range(0, enemyPrefab.Length)], 
                                    new Vector2(Camera.main.transform.position.x + x,
                                                Camera.main.transform.position.y + y
                                                ), 
                                    Quaternion.identity);

            Game.Instance.AddEnemy();
        }
    }
}