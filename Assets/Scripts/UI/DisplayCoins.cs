﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayCoins : MonoBehaviour
{
    [SerializeField]
    private bool totalCoins = false;
    private Text text;

    void Start()
    {
        text = gameObject.GetComponent<Text>();
        text.text = "0";
    }

    void Update()
    {
        text.text = totalCoins ? Game.Instance.SessionCoins.ToString() : Game.Instance.TotalCoins.ToString();
    }
}
