using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject UiGroup;
    private bool IsVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        UiGroup.SetActive(IsVisible);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && IsVisible)
        {
            ResumeGame();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && !IsVisible)
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        IsVisible = true;
        UiGroup.SetActive(IsVisible);
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        IsVisible = false;
        UiGroup.SetActive(IsVisible);
        Time.timeScale = 1;
    }
}