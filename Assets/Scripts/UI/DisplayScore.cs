using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    private Text text;

    void Start()
    {
        text = gameObject.GetComponent<Text>();
        text.text = "0";
    }

    void Update()
    {
        text.text = $"{Game.Instance.Score}";
    }
}
