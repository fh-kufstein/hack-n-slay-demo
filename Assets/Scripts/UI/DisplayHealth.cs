using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHealth : MonoBehaviour
{
    private Text text;

    void Start()
    {
        text = gameObject.GetComponent<Text>();
        text.text = "100%";
    }

    void Update()
    {
        text.text = $"{Game.Instance.Player.GetHealthPercentage()}%";
    }
}
