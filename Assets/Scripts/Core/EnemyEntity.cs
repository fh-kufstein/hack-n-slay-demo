﻿using UnityEngine;

public abstract class EnemyEntity : ScriptableObject
{
    [SerializeField]
    private string entityName;
    [SerializeField]
    private Sprite icon;
    [SerializeField]
    private RuntimeAnimatorController animationController;
    [SerializeField]
    private int attackDamage;
    [SerializeField]
    private int attackSpeed;
    [SerializeField]
    private float lifePoints;
    [SerializeField]
    private float armor;
    [SerializeField]
    private float moveSpeed;

    public string EntityName { get => entityName; protected set => entityName = value; }
    public Sprite Icon { get => icon; protected set => icon = value; }
    public RuntimeAnimatorController AnimationController { get => animationController; protected set => animationController = value; }
    public int AttackDamage { get => attackDamage; protected set => attackDamage = value; }
    public int AttackSpeed { get => attackSpeed; protected set => attackSpeed = value; }
    public float LifePoints { get => lifePoints; protected set => lifePoints = value; }
    public float Armor { get => armor; protected set => armor = value; }
    public float MoveSpeed { get => moveSpeed; protected set => moveSpeed = value; }

    protected EnemyEntity(float lifePoints, float armor, int attackSpeed, int damage, float moveSpeed)
    {
        this.LifePoints = lifePoints;
        this.Armor = armor;
        this.AttackDamage = damage;
        this.AttackSpeed = attackSpeed;
        this.MoveSpeed = moveSpeed;
    }
}
