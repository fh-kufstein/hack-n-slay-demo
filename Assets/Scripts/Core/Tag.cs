﻿public enum Tag
{
    Rifle,
    RingOfFire,
    Shotgun,
    Player,
    Enemy
}
