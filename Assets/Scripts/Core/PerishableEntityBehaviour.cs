﻿using UnityEngine;

public class PerishableEntityStats
{
    public string EntityName { get; protected set; }
    public float Lifepoints { get; protected set; }
    public float MaxLifepoints { get; protected set; }
    public int AttackDamage { get; protected set; }
    public int AttackSpeed { get; protected set; }
    public float LifePoints { get; protected set; }
    public float Armor { get; protected set; }
    public float MoveSpeed { get; protected set; }

    public MovingDirection MovingDirection { get; set; }

    public PerishableEntityStats(EnemyEntity template)
    {
        this.EntityName = template.EntityName;
        this.Lifepoints = template.LifePoints;
        this.MaxLifepoints = template.LifePoints;
        this.Armor = template.Armor;
        this.AttackDamage = template.AttackDamage;
        this.AttackSpeed = template.AttackSpeed;
        this.MoveSpeed = template.MoveSpeed;
    }

    public PerishableEntityStats(string entityName, float lifepoints, int attackDamage, int attackSpeed, float lifePoints, float armor, float moveSpeed)
    {
        this.EntityName = entityName;
        this.Lifepoints = lifepoints;
        this.MaxLifepoints = lifepoints;
        this.AttackDamage = attackDamage;
        this.AttackSpeed = attackSpeed;
        this.LifePoints = lifePoints;
        this.Armor = armor;
        this.MoveSpeed = moveSpeed;
    }

    public void TakeDamage(float dmg)
    {
        Debug.Log($"|{EntityName}| Damage: " + dmg);
        Lifepoints = Lifepoints - GetDamageAfterReduction(dmg);
        Debug.Log($"|{EntityName}| LifePoints: " + Lifepoints);
    }

    public bool IsDead()
    {
        return Lifepoints <= 0;
    }

    public int GetHealthPercentage()
    {
        return Mathf.RoundToInt(Lifepoints / MaxLifepoints * 100);
    }

    private float GetDamageAfterReduction(float dmg)
    {
        var dmgAfterArmorReduction = dmg - Armor;

        Debug.Log($"|{EntityName}| Damage After Reduction: " + dmgAfterArmorReduction);
        if (dmgAfterArmorReduction <= 0)
        {
            Debug.Log($"|{EntityName}| Damage Default (Too much Reduction)!");
            return 10;
        }
        return dmgAfterArmorReduction;
    }
}
