using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    void Start()
    {
    }

    void Update()
    {
        var distance = Vector2.Distance(transform.position, Camera.main.transform.position);

        if(distance > Game.Instance.Player.ShootingRange)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Enemy.ToString()))
        {
            Destroy(gameObject); //delete bullet
            collision.gameObject.GetComponent<EnemyBehaviour>().OnDamage();
        }
    }
}
