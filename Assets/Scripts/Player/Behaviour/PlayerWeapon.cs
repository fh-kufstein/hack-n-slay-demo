using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    [SerializeField]
    private GameObject bulletTemplate;

    private const float baseSpeed = 500;

    void Start()
    {
        StartCoroutine(Shoot());
    }

    void Update()
    {
    }

    IEnumerator Shoot()
    {
        while(true)
        {
            yield return new WaitForSeconds(.01f / Game.Instance.Player.ShootingSpeed);
            var direction = Game.Instance.Player.MovingDirection == MovingDirection.Left ? 1 : -1;


            switch (Game.Instance.Player.ShootingMode)
            {
                default:
                case ShootingMode.Rifle:
                    CreateBullet(right: baseSpeed * direction);
                    break;
                case ShootingMode.Shotgun:
                    CreateBullet(right: baseSpeed * direction);
                    CreateBullet(right: baseSpeed * direction, up: baseSpeed / 2);
                    CreateBullet(right: baseSpeed * direction, up: baseSpeed / 4);
                    CreateBullet(right: baseSpeed * direction, down: baseSpeed / 2);
                    CreateBullet(right: baseSpeed * direction, down: baseSpeed / 4);
                    break;
                case ShootingMode.RingOfFire:
                    float bullets = 15;
                    float rotationIncrement = Mathf.PI * 2.0f / bullets;
                    float rotationOffset = 1;
                    for (int i = 0; i < bullets; i++)
                    {
                        float horizontal = Mathf.Cos(rotationIncrement * i + rotationOffset) * baseSpeed;
                        float vertical = Mathf.Sin(rotationIncrement * i + rotationOffset) * baseSpeed;

                        CreateBullet(up: vertical, right: horizontal);
                        CreateBullet(down: vertical, left: horizontal);
                    }
                    break;
            }
        }
    }

    private void CreateBullet(float up = 0, float down = 0, float right = 0, float left = 0)
    {
        var bullet = Instantiate(bulletTemplate);
        bullet.transform.SetParent(transform, false);
        var rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.right * right);
        rb.AddForce(Vector2.down * down);
        rb.AddForce(Vector2.up * up);
        rb.AddForce(Vector2.left * left);
    }
}
