using UnityEngine;

public class PlayerPickup : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Rifle.ToString()))
        {
            if(Game.Instance.Player.ShootingMode <= ShootingMode.Rifle)
            {
                Game.Instance.Player.ShootingMode = ShootingMode.Rifle;
            }
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag(Tag.Shotgun.ToString()))
        {
            if (Game.Instance.Player.ShootingMode <= ShootingMode.Shotgun)
            {
                Game.Instance.Player.ShootingMode = ShootingMode.Shotgun;
            }
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag(Tag.RingOfFire.ToString()))
        {
            if (Game.Instance.Player.ShootingMode <= ShootingMode.RingOfFire)
            {
                Game.Instance.Player.ShootingMode = ShootingMode.RingOfFire;
            }
            Destroy(collision.gameObject);
        }      
    }
}
