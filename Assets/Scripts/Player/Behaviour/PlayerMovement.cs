using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D player;

    void Start()
    {
        player = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Run();
    }

    private void Run()
    {
        float horizontalInput = Input.GetAxis(Axis.Horizontal.ToString());
        float verticalInput = Input.GetAxis(Axis.Vertical.ToString());
        //Makes sure that running in more than one direction does not "boost" character
        float balancer = verticalInput != 0 && horizontalInput != 0 ? 0.75f : 1;

        player.velocity = new Vector2(horizontalInput * Game.Instance.Player.MoveSpeed * balancer,
                                        verticalInput * Game.Instance.Player.MoveSpeed * balancer);

        if (horizontalInput != 0)
        {
            Game.Instance.Player.MovingDirection = horizontalInput > 0 ? MovingDirection.Left : MovingDirection.Right;
        }

        gameObject.GetComponentInChildren<SpriteRenderer>().flipX = (int)Game.Instance.Player.MovingDirection > 1;
    }
}
