using System;
using UnityEngine;

public class Player : PerishableEntityStats
{
    public ShootingMode ShootingMode { get; set; }
    public float ShootingSpeed { get; private set; }
    public float ShootingRange { get; private set; }
    public float CoinPickupRange { get; private set; }

    public Player() : base("Player", 100, 100, 10, 1, 0, 7)
    {
        ShootingSpeed = 0.1f;
        ShootingRange = 10f;
        CoinPickupRange = 5f;
    }
}
