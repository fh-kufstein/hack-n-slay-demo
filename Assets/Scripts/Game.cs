﻿using System;
using System.Threading;

public class Game
{
    private static readonly Lazy<Game> lazy = new Lazy<Game>(() => new Game());
    private int currentEnemyAmount = 0;
    private int score = 0;
    private int totalCoins = 0;
    private int sessionCoins = 0;

    public static Game Instance { get { return lazy.Value; } }

    public Player Player { get; set; } = new Player();
    public int MaxEnemyAmount { get; private set; } = 50;
    public int SpawnSpeed { get; private set; } = 1;
    public int SpawnDistance { get; private set; } = 20; 
    public int CurrentEnemyAmount { get => currentEnemyAmount; }
    public int Score { get => score; }
    public int SessionCoins { get => sessionCoins; }
    public int TotalCoins { get => totalCoins; }
    public bool IsPaused { get; set; }

    public void RemoveEnemy()
    {
        Interlocked.Decrement(ref currentEnemyAmount);
        Interlocked.Increment(ref score);
        if(UnityEngine.Random.Range(0f, 1f) > 0.5)
        {
            MaxEnemyAmount++;
        }
    }
    public void AddEnemy()
    {
        Interlocked.Increment(ref currentEnemyAmount);
    }

    public void CollectCoin()
    {
        Interlocked.Increment(ref totalCoins);
        Interlocked.Increment(ref sessionCoins);
    }

    public void StartGame()
    {
        Player = new Player();
        score = 0;
    }
}
