using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehaviour : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    void Start()
    {
        
    }

    void Update()
    {
        if(Vector2.Distance(transform.position, Camera.main.transform.position) < Game.Instance.Player.CoinPickupRange)
        {
            transform.position = Vector2.MoveTowards(transform.position, Camera.main.transform.position, moveSpeed * Time.deltaTime);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Player.ToString()))
        {
            Destroy(gameObject);
            Game.Instance.CollectCoin();
        }
    }
}
